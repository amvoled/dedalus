'use strict';

var local = {
    world: null,
    zone: null,
    time: "afternoon",
    exitMask: null,
    exitMaskHistory: {}
};

var player = {
    pv: 10,
    maxpv: 10,
    st: 10,
    maxst: 10,
    setpv: function(change) {
	this.pv += change;
	if (this.pv > this.maxpv) {this.pv = this.maxpv};
	if (this.pv < 0) {this.pv = 0};
    },
    setst: function(change) {
	this.st += change;
	if (this.st > this.maxst) {this.st = this.maxst};
	if (this.st < 0) {this.st = 0};
    }
};

var log = {
    logLimit: 10,
    logTab: [],
    add: function(string) {
	log.logTab.push(string);
	while (log.logTab.length > log.logLimit) {log.logTab.shift()};
	log.display();
    },
    reset: function(string) {
	log.logTab = [];
	log.display();
    },
    display: function() {
	var elem = document.getElementById("log");
	elem.innerHTML = "";
	for (var i = log.logTab.length - 1; i >= 0; i--) {
	    var tmp = document.createElement("span");
	    tmp.innerHTML = log.logTab[i] + '</br>';
	    elem.appendChild(tmp);
	};
    }
};

var dedalusApp = angular.module('dedalusApp', [])
var dedalusCtrl = dedalusApp.controller('dedalusCtrl', function($scope, $compile) {
    $scope.loc = local;
    $scope.player = player;
});

var worldList = [forest];

function refresh() {
    if (document.readyState === "complete") {
	angular.element(document.getElementById('main')).scope().$apply();
    };
};

function addButton(name, elem, func) {
    var element = document.createElement("span");
    element.innerHTML = '<button onclick="' + func + '" >' + name + '</button>';
    var tmp = document.getElementById("walkto");
    tmp.appendChild(element);
    refresh();
};

function randomWorld(except) {
    do {
	var jumpTo = Math.floor(Math.random() * worldList.length) + 1;
	local.world = worldList[jumpTo - 1];
    } while (local.world.name === except);
    log.add("Player jumped to : " + local.world.name);
    var jumpto = Math.floor(Math.random() * local.world.startLocation.length) + 1;
    local.zone = local.world.startLocation[jumpto - 1];
    log.add("Player arrived at : " + local.zone.name);
    initExitList();
    exitList();
    refresh();
};

function initExitList() {
    local.exitMask = Array(local.zone.accessLocations.length);
    for (var i = 0; i < local.zone.accessLocations.length; i++) {
	if (local.zone.accessLocations[i][2] === 1) {
	    local.exitMask[i] = 1;
	} else { local.exitMask[i] = 0 };
    };
};

function exitList() {
    document.getElementById("walkto").innerHTML = "";
    for (var i = 0; i < local.exitMask.length; i++) {
	if (local.exitMask[i] === 1) {
	    addButton(local.zone.accessLocations[i][1], "walkto", "moveToZone(" + i + ")");
	};
    };
};

function searchForZones() {
    for (var i = 0; i < local.zone.accessLocations.length; i++) {
	if ((local.zone.accessLocations[i][2] != 0) && (local.exitMask[i] != 1)) {
	    var dice = Math.random();
	    log.add("Rolling the dice : " + dice);
	    if (dice < local.zone.accessLocations[i][2]) {
		local.exitMask[i] = 1;
	    };
	};
    };
    exitList();
};

function moveToZone(index) {
    local.exitMaskHistory[local.zone.name] = local.exitMask;
    local.zone = local.zone.accessLocations[index][0];
    if (local.exitMaskHistory.hasOwnProperty(local.zone.name)) {
	local.exitMask = local.exitMaskHistory[local.zone.name];
    } else { initExitList() };
    exitList();
    refresh();
};

randomWorld("nope");
exitList();
